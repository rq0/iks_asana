from asana import Client
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver


class Workspace(models.Model):
    """Рабочая зона"""
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(verbose_name='Название', max_length=128, blank=False, null=True)
    user = models.ForeignKey('AsanaUser', verbose_name="Пользователь", on_delete=models.DO_NOTHING, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Рабочая зона"
        verbose_name_plural = "Рабочие зоны"


@receiver(pre_save, sender=Workspace, dispatch_uid="workspace_update")
def workspace_update_signal(sender, instance, **kwargs):
    """Обновление данных в asana после изменения Workspace"""
    client = Client.access_token(instance.user.asana_token)
    client.workspaces.update(instance.id, {'name': instance.name})


class Project(models.Model):
    """Проект"""
    id = models.BigAutoField(primary_key=True)
    workspace = models.ForeignKey(
        'Workspace', verbose_name="Рабочая зона", blank=False, null=True, on_delete=models.DO_NOTHING)
    name = models.CharField(verbose_name='Название', max_length=128, blank=False, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Проект"
        verbose_name_plural = "Проекты"


@receiver(pre_save, sender=Project, dispatch_uid="project_update")
def project_update_signal(sender, instance, **kwargs):
    """Обновление данных в asana после изменения Project"""
    client = Client.access_token(instance.workspace.user.asana_token)
    if not instance.id:
        asana_project = client.projects.create({'name': instance.name, 'workspace': str(instance.workspace_id)})
        instance.id = asana_project.get('gid')
        return
    client.projects.update(instance.id, {'name': instance.name})


class Task(models.Model):
    """Задача"""
    id = models.BigAutoField(primary_key=True)
    project = models.ForeignKey('Project', verbose_name="Проект", blank=False, null=True, on_delete=models.DO_NOTHING)
    name = models.CharField(verbose_name='Название', max_length=128, blank=False, null=True)
    assignee = models.ForeignKey('AsanaUser', verbose_name="Назначенный", on_delete=models.DO_NOTHING, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Задача"
        verbose_name_plural = "Задачи"


@receiver(pre_save, sender=Task, dispatch_uid="task_update")
def task_update_signal(sender, instance, **kwargs):
    """Обновление данных в asana после изменения Task"""
    client = Client.access_token(instance.project.workspace.user.asana_token)
    if instance.id:
        old_task = Task.objects.get(id=instance.id)
        if old_task.project_id != instance.project_id:
            client.tasks.add_project(instance.id, {'project': str(instance.project_id)})
            client.tasks.remove_project(instance.id, {'project': str(old_task.project_id)})
    else:
        asana_task = client.tasks.create({'name': instance.name, 'projects': [str(instance.project_id)]})
        instance.id = asana_task.get('gid')
        return
    client.tasks.update(instance.id, {'name': instance.name})


class AsanaUser(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.OneToOneField(get_user_model(), verbose_name="Пользователь", on_delete=models.CASCADE)
    asana_token = models.CharField(verbose_name='API-токен доступа asana', max_length=128, blank=True, null=True)

    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name = "Пользователь Asana"
        verbose_name_plural = "Пользователи Asana"
