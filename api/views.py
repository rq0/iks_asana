from asana import Client
from django.http import HttpResponseRedirect, Http404
from django.urls import reverse
from django.views import View

from api.models import Workspace, Project, Task, AsanaUser


class SyncFromAsana(View):
    client = None

    def get(self, request, *args, **kwargs):
        asana_user_qs = AsanaUser.objects.filter(user_id=self.request.user.id)
        if not asana_user_qs:
            raise Http404
        self.asana_user = asana_user_qs[0]

        self.client = Client.access_token(self.request.user.asanauser.asana_token)

        client_asana_user = self.client.users.me()
        if self.asana_user.id != client_asana_user.get('gid'):
            asana_user_qs.update(id=client_asana_user.get('gid'))
            self.asana_user.id = client_asana_user.get('gid')

        workspaces = self.get_workspaces()
        self.add_workspaces(workspaces)

        projects = []
        for workspace in workspaces:
            projects = projects + self.get_projects(workspace.id)
        self.add_projects(projects)

        tasks = []
        for project in projects:
            tasks = tasks + self.get_tasks(project.id)
        self.add_tasks(tasks)

        return HttpResponseRedirect(reverse('admin:index'))

    def get_tasks(self, project_id):
        tasks_generator = self.client.tasks.find_by_project(project_id, {'opt_fields': ['name', 'assignee']})
        tasks = []
        try:
            while True:
                task = next(tasks_generator)
                tasks.append(
                    Task(
                        pk=task.get('gid'),
                        name=task.get('name'),
                        project_id=project_id,
                        assignee_id=task.get('assignee') and task.get('assignee').get('gid') or None
                    ))
        except StopIteration:
            return tasks

    def add_tasks(self, tasks):
        exist_task_qs = Task.objects.filter(id__in=map(lambda task: task.id, tasks))
        if exist_task_qs:
            task_list_to_create = self.filter_exists_entities(exist_task_qs, tasks)
            Task.objects.bulk_create(task_list_to_create)
        else:
            Task.objects.bulk_create(tasks)

    def get_projects(self, workspace_id):
        projects_generator = self.client.projects.find_all({
            'workspace': workspace_id
        })
        projects = []
        try:
            while True:
                project = next(projects_generator)
                projects.append(
                    Project(
                        pk=project.get('gid'),
                        name=project.get('name'),
                        workspace_id=workspace_id
                    ))
        except StopIteration:
            return projects

    def add_projects(self, projects):
        exist_project_qs = Project.objects.filter(id__in=map(lambda project: project.id, projects))
        if exist_project_qs:
            project_list_to_create = self.filter_exists_entities(exist_project_qs, projects)
            Project.objects.bulk_create(project_list_to_create)
        else:
            Project.objects.bulk_create(projects)

    def get_workspaces(self):
        workspaces_generator = self.client.workspaces.find_all()
        workspaces = []
        try:
            while True:
                workspace = next(workspaces_generator)
                workspaces.append(
                    Workspace(
                        pk=workspace.get('gid'),
                        name=workspace.get('name'),
                        user_id=self.asana_user.id
                    ))
        except StopIteration:
            return workspaces

    def add_workspaces(self, workspaces):
        exist_workspace_qs = Workspace.objects.filter(id__in=map(lambda workspace: workspace.id, workspaces))
        if exist_workspace_qs:
            workspace_list_to_create = self.filter_exists_entities(exist_workspace_qs, workspaces)
            Workspace.objects.bulk_create(workspace_list_to_create)
        else:
            Workspace.objects.bulk_create(workspaces)

    @staticmethod
    def filter_exists_entities(exist_qs, external_qs):
        exist_id_list = list(map(
            lambda entity:
            str(entity.id),
            exist_qs))
        return list(filter(
            lambda entity:
            entity.id not in exist_id_list,
            external_qs))
