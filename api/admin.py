from django.contrib import admin

# Register your models here.
from api.models import *

admin.site.register(AsanaUser)

admin.site.register(Workspace)
admin.site.register(Project)
admin.site.register(Task)
